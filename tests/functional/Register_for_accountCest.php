<?php

use App\User;
use Page\Functional\RegisterPage as RegisterPage;
use Faker\Factory as Faker;
use Step\Functional\Registration as RegistrationStep;
use CrazyInventor\Mailtrapper as CazyInventor;
use Page\Functional\LoginPage as LoginPage;
use Page\Functional\RegisterCapture as RegCapPage;


class Register_for_accountCest
{

    private $emailAddress;
    private $password;
    private $name;
    private $email_hash;

    public function _before()
    {
        //setting up faker data constants
        $faker = Faker::create();
        #todo: edit email address to it is the same as first and last names. Or generify email address.
        $this->emailAddress = $faker->unique()->safeEmail;
        $this->password = bcrypt($faker->password);
        $this->email_hash = md5($this->emailAddress);
        //clear inbox
        $mt = new CazyInventor('f548e04289976189b0395ce9fc311bbc');
        $mt->clearInbox('256250');
    }

    /**@test**/
    public function new_user_registers_for_account_via_confirming_email(FunctionalTester $I, RegisterPage $registerPage, RegistrationStep $registration,
    LoginPage $loginPage, RegCapPage $capture)
    {
        //submit register form
        $faker = Faker::create();
        $registerPage->submitRegisterForm([
                'first_name' => $this->name = $faker->firstName,
                'middle_initial' => strtoupper($faker->randomLetter),
                'last_name' => $faker->lastName,
                'phone_number' => $faker->phoneNumber,
                'job_title' => $faker->jobTitle,
                'company_name' => $faker->company,
                'address_1' => $faker->streetAddress,
                'address_2' => $faker->secondaryAddress,
                'city' => $faker->city,
                //'state_id' => $faker->stateAbbr,
                'zipcode' => $faker->postcode,
                //'country_id' => rand(1, 200),
                'email' => $this->emailAddress,
                'email_confirm' => $this->emailAddress,
                'password' => $this->password,
                'password_confirm' => $this->password]
        );

        //verify user object has contentCe
        $userObj = Auth::user();
        $I->isSet($userObj);

        //Grab registrant's confirmation email
        $registration->verify_Confirmeation_Registration_Email($this->emailAddress);
        $registration->verify_account_via_email_link($I);

        //verify user gets welcoming email after confirming account - via registration email link
        $registration->verify_Welcome_Registration_Email($this->emailAddress);

        //see user in db with a registered account
        $I->seeRecord('users', ['first_name' => $this->name, 'has_confirmed' => 1]);
        echo Auth::user();

        // Directed to /register/capture form
        $capture->submitRegCapForm();

        // Now user is completely logged in
        $I->seeCurrentUrlEquals('/courses');
    }
}
