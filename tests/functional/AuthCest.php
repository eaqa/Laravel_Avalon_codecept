<?php

use App\User;
use Page\Functional\RegisterPage as RegisterPage;
use Faker\Factory as Faker;
use Page\Functional\LoginPage as LoginPage;
use Page\Functional\RegisterCapture as RegCapPage;

class AuthCest
{

    private $emailAddress;
    private $password;
    private $name;

    public function _before()
    {
        $faker = Faker::create();
        $this->emailAddress = $faker->unique()->safeEmail;
        $this->password = bcrypt($faker->password);

    }

    /**@test**/
    public function a_user_may_register_for_an_account_but_must_confirm_their_email_address(FunctionalTester $I, RegisterPage $registerPage, RegCapPage $capture)
    {

        // When we register ...

        $faker = Faker::create();
        $registerPage->submitRegisterForm([
           'first_name' => $this->name = $faker->firstName,
            'middle_initial' => strtoupper($faker->randomLetter),
            'last_name' => $faker->lastName,
            'phone_number' => $faker->phoneNumber,
            'job_title' => $faker->jobTitle,
            'company_name' => $faker->company,
            'address_1' => $faker->streetAddress,
            'address_2' => $faker->secondaryAddress,
            'city' => $faker->city,
            //'state_id' => $faker->stateAbbr,
            'zipcode' => $faker->postcode,
            //'country_id' => rand(1, 200),
            'email' => $this->emailAddress,
            'email_confirm' => $this->emailAddress,
            'password' => $this->password,
            'password_confirm' => $this->password]
        );
        echo Auth::user();
        // We should have an account - but on that is not yet confirmed/verified.
        $I->see('Thank you for registering!');
        $I->seeLink('Click here to resend email confirmation');
        $I->seeRecord('users', ['first_name' => $this->name, 'has_confirmed' => 0]);

        $user = User::whereFirstName($this->name)->first();
        echo $user;


        //Cant Login until you confirm your email
        $this->steps_preAuth_expect_to_be_redirected_to_home($I, $user);
        echo Auth::user();

        //Like this...
        #adding confirmaiton token to $user + adds session token
        $I->amOnPage("/account/confirm/$user->confirmation_token");
        #see $user with confirmation
        $I->seeRecord('users', ['first_name' => $this->name, 'has_confirmed' => 1]);
        echo Auth::user();

        // Log a user into the application with the $id of $user.
        $id = Auth::user()->id;
        Auth::loginUsingId($id, true);
        #todo: fix errors with VerifyCsrfToken.php TokenMissMatch excepotion error. If I change my .env to #APP_ENV=local.
        #fixed by chanageing to #APP_ENV=testsing can login without exception.

        // Now can login with verified account
        $this->login($I, $user);

        // Directed to /register/capture form
        $capture->submitRegCapForm();

        // Now user is completely logged in
        $I->seeCurrentUrlEquals('/courses');
    }


    // Since user does not have an authenticated account, they are redirected by middleware to homepage
    protected function steps_preAuth_expect_to_be_redirected_to_home(FunctionalTester $I, $user = null)
    {
        $user = $user ?: \FactoryEloc()->create('App\User', ['password' => 'password']);

        $I->amOnPage(LoginPage::$URL);
        $I->seeCurrentUrlEquals('/');
        ///$I->click('Courses');
        //$I->seeInCurrentUrl('/register/success');
        //todo: need to do assertion of why I was redirected due to the lack of pre-auth
        return;
    }

    // Login form submit
    protected function login(FunctionalTester $I, $user = null)
    {

        $user = $user ?: \factoryEloc()->create('App\User', ['password' => 'password']);

        $I->amOnPage(LoginPage::$URL);
        $I->fillField('#email', $this->emailAddress);
        $I->fillField('#password', $this->password);
        $I->click('Login');
        $I->dontSeeFormErrors();
        return;
    }



}
