<?php
namespace Helper;


// here you can define custom actions
// all public methods declared in helper class will be available in $I

class Functional extends \Codeception\Module
{

    public function isSet($obj)
    {
        $this->assertTrue(isset($obj), "this thing is set");
    }

    public function seeResponseContains($text)
    {
        $this->assertContains($text, $this->getModule('Laravel5')->_getResponseContent(), "response contains");
    }

    public function loadPage($uri)
    {
        $this->getModule('Laravel5')->_loadPage('GET', $uri);
    }


}
