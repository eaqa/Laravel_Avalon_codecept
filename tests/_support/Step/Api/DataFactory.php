<?php
namespace Step\Api;


class DataFactory extends \ApiTester
{
    public function createUsers($amount, $seed)
    {

        $json = file_get_contents('http://api.randomuser.me/?nat=US&results=' . $amount . '&seed=' . $seed);
        $users = json_decode($json, true);
        print_r($users);

        foreach ($users['results'] as $user){
            $userinfo = array(
                'username' => $user['name']['first'] . '_' . $user['name']['last'],
                'password' => $user['login']['password'],
                //email will be created in another function
                'firstname' => $user['name']['first'],
                'lastname' => $user['name']['last'],
                'picture' => $user['picture'],
                'gender' => $user['gender'],
                'phone' => $user['phone'],
                'street' => $user['location']['street'],
                'city' => $user['location']['city'],
                'postcode' => $user['location']['postcode']
            );
           // print_r($userinfo[$index]);
            return $userinfo;
            // returns a populated array of user data with the above keys
        }
    }



}

