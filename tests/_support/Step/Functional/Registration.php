<?php

namespace Step\Functional;
use CrazyInventor\Mailtrapper as CazyInventor;
use PHPUnit_Framework_Assert as assert;


class Registration extends \FunctionalTester
{

    public function getEmail()
    {
        $I = $this->tester;
        $mt = new CazyInventor('f548e04289976189b0395ce9fc311bbc');
        $response = $mt->getMails('256250');
        $response = json_decode($response);
        $output = $response[0];
        print_r($output);
        //echo $arr->id;

        \PHPUnit_Framework_Assert::assertNotEmpty($output, 'Output is empty');
        return $output;
    }

    public function verify_Confirmeation_Registration_Email($to_email)
    {
        $I = $this->tester;
        $data = $this->getEmail();
        assert::assertEquals($data->subject, 'Confirm Your Account');
        assert::assertEquals($data->to_email, $to_email);
    }

    public function verify_account_via_email_link(\FunctionalTester $I)
    {
        //echo $data->html_body;
        $confirmation_link = $this->grab_confirmation_link_from_email();
        echo $confirmation_link;
        $I->loadPage($confirmation_link);
    }

    public function verify_Welcome_Registration_Email($to_email)
    {
        $data = $this->getEmail();
        assert::assertEquals($data->subject, 'Welcome to Questex University');
        assert::assertEquals($data->to_email, $to_email);

    }










    public function grab_confirmation_link_from_email()
    {
        $data = $this->getEmail();
        $dom = new \DOMDocument();
        $dom->loadHTML($data->html_body);
        foreach ($dom->getElementsByTagName('a') as $node) {
            $confirmation_link = $node->getAttribute('href');
            return $confirmation_link;
        }
    }

    /**
     * @var \FunctionalTester;
     */
    protected $tester;

    public function __construct(\FunctionalTester $I)
    {
        $this->tester = $I;
    }



}

