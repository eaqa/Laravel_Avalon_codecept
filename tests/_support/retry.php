<?php

/*class RetriesExceededException extends \Exception {}

class Retry
{

    const MAX_RETRIES = 5;

    const BASE_RETRY_DELAY = 1000000;

    static public function delayed(callable $action, $retries = 1)
    {
        if ( ! is_int($retries) || $retries > self::MAX_RETRIES) {
            throw new \InvalidArgumentException();
        }

        return self::attempt($action, $retries, true);
    }

    static public function immediately(callable $action, $retries = 1)
    {
        if ( ! is_int($retries) || $retries > self::MAX_RETRIES) {
            throw new \InvalidArgumentException();
        }

        return self::attempt($action, $retries, false);
    }

    static private function attempt(callable $action, $retries, $delay)
    {
        do {
            try {
                return $action();
            } catch (\Exception $e) {
                if ($delay && $retries) {
                    //do delay
                    sleep($delay);
                }
            }
        } while ($retries--);

        throw new RetriesExceededException();
    }

}

//Retry::delayed($delay, $action, $retries);
//Retry::immediately($action, $retries);*/

//if (!function_exists('retry')) {
//    /**
//     * Retries callable
//     *
//     * Could be specified how many times, default is 1 times.
//     *
//     * @param callable $what
//     * @param int $retry how many time should it be retried, default is 1
//     * @return mixed
//     * @throws Exception
//     */
//    function retry(callable $what, $retry = 1)
//    {
//        again:
//        try {
//            return $what();
//        } catch (\RetryableException $e) {
//            if ($retry-- > 0) {
//                goto again;
//            }
//            throw $e;
//        }
//    }
//
//    /**
//     * Class RetryableException
//     * To be able to specify specific conditions for repeating
//     */
//    class RetryableException extends \Exception{}
//}


/*
 * Retry function for e.g. external API calls
 *
 * Will try the risky API call, and retries with an ever increasing delay if it fails
 * Throws the latest error if $maxRetries is reached,
 * otherwise it will return the value returned from the closure.
 *
 * You specify the exceptions that you expect to occur.
 * If another exception is thrown, the script aborts
 *
 */
function retry(callable $callable, $expectedErrors, $maxRetries = 5, $initialWait = 1.0, $exponent = 2)
{
    if (!is_array($expectedErrors)) {
        $expectedErrors = [$expectedErrors];
    }
    try {
        return call_user_func($callable);
    } catch (Exception $e) {
        // get whole inheritance chain
        $errors = class_parents($e);
        array_push($errors, get_class($e));
        // if unexpected, re-throw
        if (!array_intersect($errors, $expectedErrors)) {
            throw $e;
        }
        // exponential backoff
        if ($maxRetries > 0) {
            usleep($initialWait * 1E6);
            return retry($callable, $expectedErrors, $maxRetries - 1, $initialWait * $exponent);
        }
        // max retries reached
        throw $e;
    }
}