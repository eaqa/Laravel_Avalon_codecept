<?php
namespace Page\Functional;
use FunctionalTester;

class RegisterPage
{
    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */

    // include url of current page
    public static $URL = '/register';

    public static function route($param)
    {
        return static::$URL.$param;
    }


    /**
     * @var \FunctionalTester;
     */
    protected $tester;

    public function __construct(FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public static $formfields =
        [
            'first_name' => '#first_name',
            'middle_initial' => '#middle_initial',
            'last_name' => '#last_name',
            'phone_number' => '#phone_number',
            'job_title' => '#job_title',
            'company_name' => '#company_name',
            'address_1' => '#address_1',
            'address_2' => '#address_2',
            'city' => '#city',
            //'state_id' => '#state_id',
            'zipcode' => '#zipcode',
            //'country_id' => '#country_id',
            'email' => '#email',
            'email_confirm' => '#email_confirm',
            'password' => '#password',
            'password_confirm' => '#password_confirm'
        ];



    public function submitRegisterForm($fields = [])
    {
        $I = $this->tester;
        $I->amOnPage(RegisterPage::$URL);
        $this->fillFormFields($fields);
        $firstName = $I->grabValueFrom('#first_name');
        $I->click('Submit');
        $I->dontSeeFormErrors();
        $I->seeCurrentUrlEquals('/register/success');
        $I->seeEventTriggered('App\Events\UserRegistrationEvent');
        return $firstName;
    }

    protected function fillFormFields($data)
    {
        foreach ($data as $field => $value) {
            if (!isset(static::$formfields[$field])) {
                throw new \Exception("Form field $field does not exist");
            }
            $this->tester->fillField(static::$formfields[$field], $value);
        }
    }

}
