<?php
namespace Page\Functional;

use Codeception\Util\Locator;

class RegisterCapture
{
    // include url of current page
    public static $URL = '/register/capture';

    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */
    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \FunctionalTester;
     */
    protected $tester;

    public function __construct(\FunctionalTester $I)
    {
        $this->tester = $I;
    }

    public function submitRegCapForm()
    {
        $field_1 = Locator::find('input', ['title' => 'Must contain only positive numbers']);
        $field_2 = Locator::find('select', ['name' => 'answers[766][]']);
        $submit_btn = Locator::find('button', ['type' => 'submit']);

        $I = $this->tester;
        $I->fillField($field_1, random_int(0, 39));
        $I->selectOption($field_2, 'AL');
        $I->dontSeeFormErrors();
        $I->click($submit_btn);
    }




}
