<?php
namespace Page\Functional;

class CoursesPage
{
    public static $url = '/courses';

    public static function route($param)
    {
        return static::$url . $param;
    }

}
