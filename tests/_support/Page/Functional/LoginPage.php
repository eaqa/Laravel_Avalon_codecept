<?php
namespace Page\Functional;

use Dompdf\Exception;
use Illuminate\Support\Facades\Auth;

class LoginPage
{
    /**
     * Declare UI map for this page here. CSS or XPath allowed.
     * public static $usernameField = '#username';
     * public static $formSubmitButton = "#mainForm input[type=submit]";
     */

    /**
     * Basic route example for your current URL
     * You can append any additional parameter to URL
     * and use it in tests like: Page\Edit::route('/123-post');
     */

    // include url of current page
    public static $URL = '/login';

    public static function route($param)
    {
        return static::$URL.$param;
    }

    /**
     * @var \FunctionalTester;
     */
    protected $tester;

    public function __construct(\FunctionalTester $I)
    {
        $this->tester = $I;
    }



    public function submitLoginForm($fields = [])
    {

        $formfields =
            [
                'email' => '#email',
                'password' => '#password'
            ];
        $I = $this->tester;
        $I->amOnPage(LoginPage::$URL);
        //$I->seeElement('//*[@id="email"]');
        $I->submitForm('//*[@id="email"]', $formfields, 'submitButton');

    }

}


